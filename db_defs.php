<?php

// Notes table definition
// id auto increment
// cid integer
// uid integer
// title varchar(256)
// contents longtext
// tags varchar(256) 	|tag1|tag2|tag3|
// shared 1/0 			shared across account?
// public 0/1
// 8018304 501144

//$sql = "CREATE TABLE IF NOT EXISTS `notes` (
//  `id` int(11) NOT NULL auto_increment,
//  `cid` int(11) NOT NULL,
//  `uid` int(11) NOT NULL,
//  `name` varchar(50) collate utf8_unicode_ci NOT NULL,
//  `shardid` tinyint(4) NOT NULL,
//  `title` varchar(256) collate utf8_unicode_ci default NULL,
//  `contents` longtext collate utf8_unicode_ci,
//  `tags` varchar(256) collate utf8_unicode_ci default NULL,
//  `shared` tinyint(4) default '1',
//  `public` tinyint(4) default '0',
//  `changed` bigint(20) default NULL,
//  PRIMARY KEY  (`id`),
//  KEY `cid` (`cid`),
//  KEY `uid` (`uid`),
//  FULLTEXT KEY `contents` (`contents`),
//) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ;";
//
//$result = $db->query($sql) or die('Could not create table.');


//$sql='CREATE TABLE IF NOT EXISTS `tags` (
//  `uid` int(11) NOT NULL,
//  `tag` varchar(64) NOT NULL,
//  `note_id` int(11) NOT NULL,
//  PRIMARY KEY (`uid`,`tag`,`note_id`),
//  KEY `note_idx` (`note_id`)
//) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;';

$db = ryDB::getInstance('app_notes');
$db->setDbDefs('notes', array(
	'id' => SQL_DEF_INT,
	'cid' => SQL_DEF_INT,
	'uid' => SQL_DEF_INT,
	'name' => SQL_DEF_TEXT,
	'shardid' => SQL_DEF_INT,
	'title' => SQL_DEF_TEXT,
	'contents' => SQL_DEF_TEXT,
	'tags' => SQL_DEF_TEXT, // unused
	'shared' => SQL_DEF_INT,
	'public' => SQL_DEF_INT,
	'changed' => SQL_DEF_INT,
));

$db->setDbDefs('tags', array(
	'uid' => SQL_DEF_INT,
	'tag' => SQL_DEF_TEXT,
	'note_id' => SQL_DEF_INT,
));

$db->setDbDefs('tickets_player', array(
	'id' => SQL_DEF_INT,
	'player_id' => SQL_DEF_TEXT,
	'player_name' => SQL_DEF_TEXT,
	'note_date' => SQL_DEF_DATE,
	'note_author' => SQL_DEF_TEXT,
	'note_message' => SQL_DEF_TEXT,
	'type' => SQL_DEF_TEXT,
));

$db->setDbDefs('tickets', array(
	'id' => SQL_DEF_INT,
	'mail_id' => SQL_DEF_TEXT,
	'user_id' => SQL_DEF_TEXT,
	'contact' => SQL_DEF_TEXT,
	'email' => SQL_DEF_TEXT,
	'char' => SQL_DEF_TEXT,
	'creation_date' => SQL_DEF_DATE,
	'update_date' => SQL_DEF_DATE,
	'subject' => SQL_DEF_TEXT,
	'message' => SQL_DEF_TEXT,
	'attachments' => SQL_DEF_TEXT,
	'assignee' => SQL_DEF_TEXT,
	'type' => SQL_DEF_TEXT,
	'channel' => SQL_DEF_TEXT,
	'from' => SQL_DEF_TEXT,
	'lang' => SQL_DEF_TEXT,
	'utf8' => SQL_DEF_INT,
));

$db->setDbDefs('tickets_notes', array(
	'id' => SQL_DEF_INT,
	'user_id' => SQL_DEF_TEXT,
	'contact' => SQL_DEF_TEXT,
	'email' => SQL_DEF_TEXT,
	'creation_date' => SQL_DEF_DATE,
	'subject' => SQL_DEF_TEXT,
	'message' => SQL_DEF_TEXT,
	'attachments' => SQL_DEF_TEXT,
	'parent_id' => SQL_DEF_INT,
	'type' => SQL_DEF_TEXT,
	'sender' => SQL_DEF_TEXT,
	'utf8' => SQL_DEF_INT,
));

$db->setDbDefs('kanboard', array(
	'id' => SQL_DEF_INT,
	'email' => SQL_DEF_TEXT,
	'task_id' => SQL_DEF_INT,
	'data' => SQL_DEF_DATE,
	'date' => SQL_DEF_DATE,
	'managed' => SQL_DEF_TEXT,
));

$db->setDbDefs('kanboard_notification', array(
	'project_id' => SQL_DEF_INT,
	'event' => SQL_DEF_TEXT,
	'columns' => SQL_DEF_TEXT,
));


$db->setDbDefs('ulukyneries', array(
	'id' => SQL_DEF_INT,
	'uid' => SQL_DEF_INT,
	'feature' => SQL_DEF_TEXT,
	'name' => SQL_DEF_TEXT,
	'guild' => SQL_DEF_INT,
	'comment' => SQL_DEF_TEXT,
));

