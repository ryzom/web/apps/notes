<?php
date_default_timezone_set('UTC');
define('APP_NAME', 'app_notes');

$bench_start = microtime(true);

// web api, translations
require_once '../config.php';
require_once '../app_index/lang.php';
require_once '../l10n.php';
require_once 'lang.php';

// notes classes
require_once './db_defs.php';
require_once './src/AppNotes/Views.php';
require_once './src/AppNotes/Notes.php';

// Manage the authentication and set globaly $ig and $user
$isAuthenticated = ryzom_auth_user();

// logs
if (_user()->inGroup('DEV:WDEV')) {
	ryLogger::getInstance()->enable = true;
}

// app notes
$dbNotes = ryDB::getInstance('app_notes');

$myNotes = new AppNotes_Notes($dbNotes, _user());
$myViews = new AppNotes_Views($myNotes);

$mode = ryzom_get_param('mode');
$id = intval(ryzom_get_param('id'));

$content = '';

if ($mode != 'del' && $mode != 'save') {
	// Make sure app_profile vars are invalid
	$_SESSION['pname'] = '';
	$_SESSION['ptype'] = '';
}

$content .= $myViews->handle($mode, $id);

p('compute in '.intval((microtime(true) - $bench_start) * 1000000).' us');

if (!RYZOM_IG) {
	$content .= '
	<style type="text/css">
		h1, h2 { margin: 0; padding: 0; }
		h5 { display: inline; font-size: 14px; font-weight: bold;}
		pre { margin: 0;}
		.ryzom-ui textarea { border-color: #000 #142 #142 #000; border-width: 1px; }
	</style>
	';
}
$content .= '<br /><br /><br />';

echo ryzom_app_render(APP_NAME, $content);
