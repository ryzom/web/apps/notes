<?php // %2019-11-28T15:53:52+01:00
$__texts = array (
  'user_pad' => 
  array (
    'en' => '%s\'s Notes',
    'fr' => 'Notes de %s',
    'de' => '%s\'s Notizen',
    'ru' => 'Примечания %',
    'es' => 'Notas de %s',
    'wk' => '%s\'s Notes',
  ),
  'back' => 
  array (
    'en' => '« Back',
    'fr' => '« Retour',
    'de' => '« Zurück',
    'ru' => '« Назад',
    'es' => '« Atrás',
    'wk' => '« Back',
  ),
  'edit_note' => 
  array (
    'en' => 'Edit note',
    'fr' => 'Éditer la note',
    'de' => 'Notiz bearbeiten',
    'ru' => 'Редактировать запись',
    'es' => 'Editar nota',
    'wk' => 'Edit note',
  ),
  'note_title' => 
  array (
    'en' => 'Title',
    'fr' => 'Titre',
    'de' => 'Titel',
    'ru' => 'Название',
    'es' => 'Título',
    'wk' => 'Title',
  ),
  'btn_save' => 
  array (
    'en' => 'Save note',
    'fr' => 'Sauver la note',
    'de' => 'Notiz speichern',
    'ru' => 'Сохранить запись',
    'es' => 'Guardar nota',
    'wk' => 'Save note',
  ),
  'btn_delete' => 
  array (
    'en' => 'Delete note',
    'fr' => 'Effacer la note',
    'de' => 'Notiz löschen',
    'ru' => 'Удалить запись',
    'es' => 'Borrar nota',
    'wk' => 'Delete note',
  ),
  'shared_for_all_characters_on_account' => 
  array (
    'en' => 'Share this note between all characters on this account.',
    'fr' => 'Partager cette note avec les autres personnages de ce compte.',
    'de' => 'Teile diese Notiz mit allen Charakteren dieses Accounts.',
    'ru' => 'Сделать запись доступной для всех персонажей этой учетной записи',
    'es' => 'Compartir esta nota entre los personajes de esta cuenta.',
    'wk' => 'Share this note between all characters on this account.',
  ),
  'you' => 
  array (
    'en' => 'You',
    'fr' => 'Vous',
    'de' => 'Dir',
    'ru' => 'Вы',
    'es' => 'Usted',
    'wk' => 'You',
  ),
  'changed' => 
  array (
    'en' => 'last changed',
    'fr' => 'Dernière modification',
    'de' => 'letzte Änderung',
    'ru' => 'последние изменения',
    'es' => 'ultimo cambio',
    'wk' => 'last changed',
  ),
  'title' => 
  array (
    'en' => 'note name',
    'fr' => 'Note',
    'de' => 'Notiz',
    'ru' => 'название записи',
    'es' => 'nombre de la nota',
    'wk' => 'note name',
  ),
  'btn_new_note' => 
  array (
    'en' => 'Add note',
    'fr' => 'Nouvelle note',
    'de' => 'Notiz hinzufügen',
    'ru' => 'Добавить запись',
    'es' => 'Agregar nota',
    'wk' => 'Add note',
  ),
  'add_note' => 
  array (
    'en' => 'New note',
    'fr' => 'Nouvelle note',
    'de' => 'Neue Notiz',
    'ru' => 'Новая запись',
    'es' => 'Nueva nota',
    'wk' => 'New note',
  ),
  'btn_yes' => 
  array (
    'en' => 'Yes',
    'fr' => 'Oui',
    'de' => 'Ja',
    'ru' => 'Да',
    'es' => 'Sí',
    'wk' => 'Yes',
  ),
  'error_del' => 
  array (
    'en' => 'An error occurred while trying to delete the note.',
    'fr' => 'Une erreur s\'est produite en essayant d\'effacer la note.',
    'de' => 'Ein Fehler trat auf beim Versuch die Notiz zu löschen.',
    'ru' => 'В процессе удаления записи произошла ошибка.',
    'es' => 'Se produjo un error mientras se trataba de borrar la nota.',
    'wk' => 'An error occurred while trying to delete the note.',
  ),
  'error_save' => 
  array (
    'en' => 'An error occurred while trying to save the note.',
    'fr' => 'Une erreur s\'est produite en essayant de sauver la note.',
    'de' => 'Ein Fehler trat auf beim Versuch die Notiz zu speichern.',
    'ru' => 'В процессе сохранения записи произошла ошибка.',
    'es' => 'Se produjo un error mientras se trataba de guadar la nota.',
    'wk' => 'An error occurred while trying to save the note.',
  ),
  'confirm_deletion' => 
  array (
    'en' => 'Are you sure you want to delete this note?',
    'fr' => 'Etes-vous sur de vouloir effacer cette note ?',
    'de' => 'Bist Du sicher, dass Du diese Notiz löschen willst?',
    'ru' => 'Вы уверены, что хотите удалить эту запись?',
    'es' => '¿De verdad quieres borrar esta nota?',
    'wk' => 'Are you sure you want to delete this note?',
  ),
  'tags' => 
  array (
    'en' => 'Tags',
    'fr' => 'Tags',
    'de' => 'Tags',
    'ru' => 'Тэги',
    'es' => 'Etiquetas',
    'wk' => 'Tags',
  ),
  'multiple_tag_info' => 
  array (
    'en' => '(separate tags with commas)',
    'fr' => 'Séparez les mots-clés avec une virgule.',
    'de' => '(mehrere Tags durch Kommata trennen)',
    'ru' => '(пишите тэги через запятую)',
    'es' => '(separe las etiquetas con comas)',
    'wk' => '(separate tags with commas)',
  ),
  'show_all' => 
  array (
    'en' => 'Show all notes',
    'fr' => 'Montrer toutes les notes',
    'de' => 'Alle Notizen anzeigen',
    'ru' => 'Показать все записи',
    'es' => 'Mostrar todas las notas',
    'wk' => 'Show all notes',
  ),
);
if(isset($ryzom_texts))
  $ryzom_texts = array_merge ($__texts, $ryzom_texts);
else
  $ryzom_texts = $__texts;
