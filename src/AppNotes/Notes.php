<?php

/**
 * Notes
 */
class AppNotes_Notes {
	/** @var null */
	public $name;

	/** @var int */
	public $cid;

	/** @var int */
	public $uid;

	/** @var int */
	public $shardid;

	/** @var \ryDB */
	private $db;

	/** @var string */
	private $sqlOwnerCheck;

	/**
	 * @param ryDB $db
	 * @param ryUser $user
	 */
	function __construct(ryDB $db, ryUser $user) {
		$this->db = $db;
		$this->cid = (int)$user->cid;
		$this->uid = (int)$user->uid;

		$this->name = $user->char_name;
		$this->shardid = (int)$user->shardid;

		$this->sqlOwnerCheck = "(`notes`.cid = {$this->cid} OR (`notes`.uid = {$this->uid} AND `notes`.shared = 1))";
	}

	/**
	 * @return bool
	 */
	function isPost() {
		return $_SERVER['REQUEST_METHOD'] === 'POST';
	}

	/**
	 * @param string $tag
	 *
	 * @return array
	 */
	function getNotes($tag) {
		if ($tag) {
			$tag = $this->db->sqlEscape($tag);
			$sql = "
			SELECT
				`id`, `cid`, `name`, `shardid`, `title`, `changed`, `tags`.`tag`
			FROM
				`notes`
			JOIN
				`tags` ON (notes.`id` = tags.`note_id` AND `tag` = '$tag')
			WHERE
				{$this->sqlOwnerCheck}
			";
		} else {
			$sql = "
			SELECT
				`id`, `cid`, `name`, `shardid`, `title`, `changed`
			FROM
				`notes`
			WHERE
				{$this->sqlOwnerCheck}
			AND
				LEFT(`title`, 1) <> '@'
			";
		}

		$result = $this->db->sqlQuery($sql, 'id', MYSQLI_ASSOC);
		return $result;
	}

	/**
	 * @param int $id
	 *
	 * @return array
	 */
	function getNote($id) {
		$note = $this->db->querySingleAssoc(
			'notes',
			array('id' => $id),
			" AND {$this->sqlOwnerCheck}"
		);
		if (is_array($note))
			$note['tags'] = $this->getNoteTags($id);
		return $note;
	}

	/**
	 * @param string $title
	 *
	 * @return array
	 */
	function getNoteByTitle($title) {
		$note = $this->db->querySingleAssoc(
			'notes',
			array('title' => $title),
			" AND {$this->sqlOwnerCheck}"
		);
		if (is_array($note)) {
			$note['tags'] = $this->getNoteTags($note['id']);
		}
		return $note;
	}

	/**
	 * @param int $id
	 *
	 * @return array|null
	 */
	function deleteNote($id) {
		if (!$this->isPost()) {
			return;
		}

		$result = $this->db->delete(
			'notes',
			array('id' => $id),
			" AND {$this->sqlOwnerCheck} LIMIT 1"
		);
		// this cleans up all tags that are not linked to any notes
		$this->db->delete('tags', array('uid' => $this->uid), ' AND NOT EXISTS (SELECT `id` FROM `notes` WHERE `tags`.`note_id` = `notes`.`id`)');
		return $result;
	}

	/**
	 * @param int $id
	 *
	 * @return bool
	 */
	function saveNote($id) {
		if (!$this->isPost()) {
			return false;
		}

		$id = (int)$id;
		$title = ryzom_get_param('title');
		$contents = str_replace('<br>', "\n", ryzom_get_param('contents'));
		$shared = (ryzom_get_param('shared') != '') ? 1 : 0;
		$tags = explode(',', ryzom_get_param('tags'));

		// strip <SPACE>, <TAB>, <CR>, <LF>
		$contents = trim($contents, " \t\n\r");
		$title = trim($title);

		if ($id == -1) {
			// Adding a new note
			$id = $this->db->insert('notes', array(
				'cid' => $this->cid,
				'uid' => $this->uid,
				'name' => $this->name,
				'shardid' => $this->shardid,
				'title' => $title,
				'contents' => $contents,
				'shared' => $shared,
				'changed' => time(),
			));
			if (!$id) {
				// insert failure
				return '';
			}
		} else {
			$this->db->delete('tags', array('note_id' => $id));

			// note: 'tags' field in 'notes' table not used
			$result = $this->db->update('notes', array(
				'title' => $title,
				'contents' => $contents,
				'shared' => $shared,
				'tags' => '',
				'changed' => time(),
			), array(
				'id' => $id,
			), " AND {$this->sqlOwnerCheck}");
			if (!$result) {
				// update failure
				return false;
			}
		}

		if ($tags == '') {
			return true;
		}

		foreach ($tags as $tag) {
			$tag = trim(substr($tag, 0, 64)); // 64 is max db field length
			if (empty($tag)) {
				continue;
			}

			$this->db->insert('tags', array(
				'uid' => $this->uid,
				'tag' => $tag,
				'note_id' => $id,
			));
		}

		return true;
	}

	/**
	 * @param int $id
	 *
	 * @return array
	 */
	function getNoteTags($id) {
		$result = $this->db->queryAssoc('tags', array(
			'uid' => $this->uid,
			'note_id' => $id,
		), 'ORDER BY `tag`', 'tag');
		return $result;
	}

	/**
	 * @param string $sortBy
	 * @param string $order
	 *
	 * @return array
	 */
	function getAllTags($sortBy = '', $order = 'ASC') {
		$sql = "
		SELECT
			`tag`, count(`note_id`) `count`
		FROM
			`tags`
		WHERE
			`uid` = $this->uid
		GROUP BY `tag`
		";

		if ($sortBy != '' && in_array($sortBy, array('tag', 'count'))) {
			$sql .= " ORDER BY `$sortBy`";
			if (strtolower($order) == 'asc' || strtolower($order) == 'desc') {
				$sql .= ' '.$order;
			}
		}

		$result = $this->db->sqlQuery($sql, 'tag', MYSQLI_ASSOC);
		return $result;
	}

	/**
	 * @param string $sel
	 *
	 * @return string
	 */
	function getTagsCloud($sel = '') {
		$all_tags = $this->getAllTags('tag', 'ASC');

		// get min, max and average
		$min = false;
		$max = false;
		foreach ($all_tags as $tag) {
			if ($min == false || $tag['count'] < $min) {
				$min = $tag['count'];
			}
			if ($max == false || $tag['count'] > $max) {
				$max = $tag['count'];
			}
		}
		$spread = $max - $min;
		if ($spread <= 0) {
			$spread = 1;
		}

		$font_smallest = ON_IPHONE ? 100 : 85;
		$font_largest = ON_IPHONE ? 200 : 135;
		$font_spread = $font_largest - $font_smallest;
		$font_step = $font_spread / $spread;

		$cloud = '';
		foreach ($all_tags as $tag) {
			/*if ($cloud != '') {
				$cloud .= ', ';
			}*/
			if ($tag['count'] == 0) {
				continue;
			}
			$tagName = _h($tag['tag']);
			/*if ($tag['tag'] == $sel) {
				$tagName = '<font color="#90ff90">'.$tagName.'</font>';
			}*/
			if ($tag['tag'] == $sel)
				$tagBg = 'style="background-color: cyan"';

			if ($tag['count'] > 1) {
				//$tagName .= '<font color="#888888">('.$tag['count'].')</font>';
				$tagName .= '('.$tag['count'].')';
			}
			//$p = ($font_smallest + (($tag['count'] - $min) * $font_step));

			$cloud .= '<a class="ryzom-ui-button" style="margin: 2px" href="'.SERVER.'?tag='.urlencode($tag['tag']).'">';
			//$cloud .= '<span style="font-size: '.$p.'%">'.$tagName.'</span>';
			$cloud .= '<span style="'.($tag['tag'] == $sel?'background-color: #005B8C;':'').' font-size: 85%">'.$tagName.'</span>';
			$cloud .= '</a>&nbsp;';
		}
		return $cloud;
	}

}
