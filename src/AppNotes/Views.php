<?php
/**
 * Views
 */
class AppNotes_Views {

	/** @var \AppNotes_Notes */
	private $Notes;

	/** @var bool */
	private $ig;

	/** @var array */
	private $shards = array(
		'101' => 'Aniro',
		'102' => 'Leanon',
		'103' => 'Arispotle',
		'301' => 'Yubo'
	);

	/** @var string */
	private $redirect;

	/**
	 * @param AppNotes_Notes $myNotes
	 */
	function __construct(AppNotes_Notes $myNotes) {
		$this->Notes = $myNotes;
		$this->ig = RYZOM_IG;

		$this->redirect = ryzom_get_param('redirect', '');
	}

	/**
	 * @param string $mode
	 * @param int $id
	 *
	 * @return string
	 */
	function handle($mode, $id) {
		$r = '';
		switch ($mode) {
		case 'add':
			$ptitle = ryzom_get_param('ptitle');
			$ptag = ryzom_get_param('ptag');
			$r .= $this->addNote($ptitle, $ptag);
			break;
		case 'del':
			$r .= $this->deleteNote($id);
			if ($r == '' && $this->redirect !== '') {
				header('Location: '.$this->redirect);
			}
			$r .= $this->listNotes();
			break;
		case 'askdel';
			$r .= $this->askDeleteNote($id);
			break;
		case 'edit':
			$title = ryzom_get_param('title');
			$r .= $this->editNote($id, $title);
			break;
		case 'edit_profile';
			$title = ryzom_get_param('title');
			$tag = ryzom_get_param('tag');
			$r .= $this->editNote($id, $title, $tag, true);
			break;
		case 'save':
			$r .= $this->saveNote($id);
			if (ryzom_get_param('embbed')) {
				ryLua::add('
					getUI("ui:interface:app_notes").active = false
					getUI("ui:interface:webig"):find("html"):refresh()
					');
				return;
			}
			if ($r == '' && $this->redirect !== '') {
				header('Location: '.$this->redirect);
			}
			$r .= $this->listNotes();
			break;
		default:
			$tag = ryzom_get_param('tag');
			$r .= $this->listNotes($tag);
			break;
		} // switch mode

		return $r;
	}

	/**
	 * @param string $tag
	 *
	 * @return string
	 */
	function listNotes($tag = '') {

		$notes = $this->Notes->getNotes($tag);

		if ($this->ig) {
			$pad = 'width="1%"';
			$tw = 'width="98%"';
			$tag_width = ' width="150" ';
			$changed_width = ' width="10%" ';
			$changed_wrap = '';
		} else {
			$pad = '';
			$tw = '';
			$changed_wrap = ON_IPHONE ? '' : 'nowrap';
			$tag_width = ON_IPHONE ? '' : ' width="200" ';
			$changed_width = ' width="18%" ';
		}

		$row_height = ON_IPHONE ? 30 : 20;

		$tableRows = '';
		if ($notes) {
			$tableRows = $this->renderNotesList($notes, array(
				'tw' => $tw,
				'row_height' => $row_height,
				'changed_wrap' => $changed_wrap,
				'changed_width' => $changed_width,
			));
		}
		$tplTable = '
		%iphone-tagcloud%
		<table style="background-color: #00000055" width="100%" cellspacing="0" cellpadding="0">
		<tr><td align="center" style="background: url(https://api.ryzom.com/data/img/bordure.png) repeat; height: 16px"><a href="#" class="ryzom-ui-button" style="background-color: black">&nbsp;-&nbsp;%users-notes%&nbsp;-&nbsp;</span></td></tr>
		<tr valign="middle"><td>
			<table width="100%">
			<tr><td>%tagcloud%</td><td align="right"><a href="%uri_all%">%_show-all%</a>&nbsp;</td></td></tr>
			</table>

		</td></tr>
		<tr valign="top" ><td width="100%">%notes%</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td>
			<form action="%uri%" method="post">
				<input type="submit" name="add" value="%_btn_new_note%">
			</form>
		</td></tr>
		<tr><td>&nbsp;</td></tr>
		<tr><td colspan="3" style="background: url(https://api.ryzom.com/data/img/bordure.png) repeat; height: 14px"></td></tr>
		</table>';

		//$tagcloud = $this->renderTagcloud($tag, array('pad' => $pad));
		$tagcloud = $this->Notes->getTagsCloud($tag);

		return strtr($tplTable, array(
			'%users-notes%' => _h(_t('user_pad', $this->Notes->name)),
			'%iphone-tagcloud%' => ON_IPHONE ? $tagcloud : '',
			'%tagcloud%' => ON_IPHONE ? '' : $tagcloud,
			'%pad%' => $pad,
			'%notes%' => $tableRows,
			'%tag_width%' => $tag_width,
			'%uri_all%' => SERVER,
			'%uri%' => SERVER.'?mode=add',
			'%_show-all%' => _h(_t('show_all')),
			'%_btn_new_note%' => _h(_t('btn_new_note')),
		));
	}

	/**
	 * @param string $ptitle
	 * @param string $ptag
	 *
	 * @return string
	 */
	function addNote($ptitle = '', $ptag = '') {
		return $this->editNote(-1, $ptitle, $ptag);
	}

	/**
	 * @param int $id
	 * @param string $ptitle
	 * @param string $ptag
	 * @param bool $embbed
	 *
	 * @return string
	 */
	function editNote($id, $ptitle = '', $ptag = '', $embbed = false) {
		$note = $this->Notes->getNote($id);

		if (!isset($note['id'])) {
			// set defaults for new note
			// Shared is on by default for new notes
			$note = array(
				'id' => -1,
				'title' => $ptitle,
				'contents' => '',
				'shared' => 1,
				'tags' => [],
			);
		}

		return $this->renderEditNote($note, $ptitle, $ptag, $embbed);
	}

	/**
	 * @param int $id
	 *
	 * @return string
	 */
	function saveNote($id) {
		$r = '';
		if (!$this->Notes->saveNote($id)) {
			$r = '<div class="error">'._t('error_save').'</div>';
		}

		return $r;
	}

	/**
	 * @param int $id
	 *
	 * @return string
	 */
	function askDeleteNote($id) {
		$note = $this->Notes->getNote($id);
		if (!$note) {
			return '<div class="error">'._t('error_del').'</div>';
		}

		return $this->renderDeleteNote($note);
	}

	/**
	 * @param int $id
	 *
	 * @return string
	 */
	function deleteNote($id) {
		$r = '';
		if (!$this->Notes->deleteNote($id)) {
			$r = '<div class="error">'._t('error_del').'</div>';
		}

		return $r;
	}

	/**
	 * @param string $tag
	 * @param array $opts array with keys (pad)
	 *
	 * @return string
	 */
	// Unused
	protected function renderTagcloud($tag = '', array $opts) {
		$tpl = '
		<table cellspacing="1" cellpadding="0" width="100%">
		<tr valign="middle" bgcolor="#000000">
			<td height="30" width="100%">
				<table cellspacing="0" cellpadding="0" width="100%">
				<tr valign="middle">
					<td height="30"><h5>%_tags%</h5></td>
				</tr>
				<tr valign="middle">
					<td align="right"><a href="%uri%">%_show-all%</a>&nbsp;</td>
				</tr>
				</table>
			</td>
		</tr>
		<tr bgcolor="#3f3f3f" valign="middle">
			<td></td>
			<td align="left">%tags-cloud%</td>
		</tr>
		</table>&nbsp;';

		return strtr($tpl, array(
			'%pad%' => $opts['pad'],
			'%uri%' => SERVER,
			'%_tags%' => _h(_t('tags')),
			'%_show-all%' => _h(_t('show_all')),
			'%tags-cloud%' => $this->Notes->getTagsCloud($tag),
		));
	}

	/**
	 * @param array $notes
	 * @param array $opts array with keys (tw, row_height, changed_wrap, changed_width)
	 *
	 * @return string
	 */
	protected function renderNotesList(array $notes, array $opts) {
		$i = 1;

		$tplRow = '
		<tr><td height="1px"></td></tr>
		<tr valign="middle" bgcolor="%bgcolor%">
			<td height="%height%" align="center">%index%</td>
			<td>%title%</td>
			<td align="center" nowrap>%name%</td>
			<td align="right" %changed_wrap%>%changed_relative%&nbsp;</td>
		</tr>
		';

		$tableRows = '';
		foreach ($notes as $note) {
			// Char info
			if ($note['cid'] != $this->Notes->cid) {
				$name = $note['name'];
				if ($note['shardid'] != $this->Notes->shardid && isset($this->shards[$note['shardid']])) {
					$name .= '('.$this->shards[$note['shardid']].')';
				}
			} else {
				$name = _h(_t('you'));
			}
			$title = sprintf('<a href="%s?mode=edit&id=%d">%s</a>',
				SERVER,
				$note['id'],
				$note['title'] ? _h($note['title']) : 'Unnamed note'
			);
			$modified = $note['changed'] ? ryzom_relative_time($note['changed'] - 1) : '(unknown)';

			$tableRows .= strtr($tplRow, array(
				'%index%' => $i,
				'%bgcolor%' => $i % 2 ? '#3f3f3f' : '#303030',
				'%height%' => $opts['row_height'],
				'%title%' => $title,
				'%name%' => $name,
				'%changed_wrap%' => $opts['changed_wrap'],
				'%changed_relative%' => $modified,
			));
			$i++;
		}

		$tplTableNotes = '
		<table width="100%" cellspacing="0" cellpadding="0">
		<tr style="background-color: #002B48; color: #8AE7FF">
			<td valign="middle" align="center" width="2%" height="%height%" nowrap>&nbsp;#&nbsp;</td>
			<td valign="middle" align="left">%_title%</td>
			<td valign="middle" align="center" nowrap>%_by%</td>
			<td valign="middle" align="right" %changed_width%%changed_wrap%>%_changed%&nbsp;</td>
		</tr>
		%table-rows%
		</table>';

		return strtr($tplTableNotes, array(
			'%height%' => $opts['row_height'],
			'%_title%' => _h(_t('title')),
			'%_by%' => _h(_t('by')),
			'%changed_width%' => $opts['changed_width'],
			'%changed_wrap%' => $opts['changed_wrap'],
			'%_changed%' => _h(_t('changed')),
			'%table-rows%' => $tableRows,
		));
	}

	/**
	 * @param array $note
	 * @param string $ptitle
	 * @param string $ptag
	 * @param bool $embbed
	 *
	 * @return string
	 */
	function renderEditNote($note, $ptitle = '', $ptag = '', $embbed = false) {
		$tags_list = [];
		$noteTags = [];
		$all_tags = $this->Notes->getAllTags('tag', 'ASC');

		if ($ptitle != '')
			$note['title'] = $ptitle;

		foreach ($note['tags'] as $tag)
			$noteTags[$tag['tag']] = true;

		foreach ($all_tags as $tag)
			$tags_list[] = _h($tag['tag']).($tag['count'] > 1?'<font color="#888888">('.$tag['count'].')</font>':'');
		$tags_list = implode(', ', $tags_list);

		if ($ptag != '')
			$noteTags[$ptag] = true;

		$tags_list .= '&nbsp;';
		$width1 = $this->ig ? '1%' : '8%';
		$width2 = $this->ig ? '1%' : '92%';


		// 'back' link and title - not visible in embbed mode
		$modestr = ($note['id'] == -1)
				? _h(_t('add_note'))
				: _h(_t('edit_note'));

		if ($embbed) {
			$strHeader = '<table width="100%"><tr><td><h2>'.$modestr.'</h2></td><td><h2>'.$note['title'].'</h2></td></tr></table>';
		} else {

			$uri = $this->redirect != '' ? $this->redirect : SERVER;

			$strHeader = $this->renderBackLink($uri);
			$strHeader .= '<table width="100%"><tr><td><h2>'.$modestr.'</h2></td><td align="right">%form-delete%</td></tr></table>';
		}

		$tpl = '
		%str-header%

		<form method="post" action="%uri%">
		<input type="hidden" name="mode" value="save" />
		<input type="hidden" name="cid" value="%cid%" />
		<input type="hidden" name="id" value="%id%" />
		<input type="hidden" name="redirect" value="%redirect%" />
		'.($embbed?'<input type="hidden" name="embbed" value="1" />':'').'

		<table width="100%" cellspacing="0" cellpadding="2">

		';

		// 'title' - not visible in embbed mode
		$tpl .= $embbed ? '
			<input type="hidden" name="title" value="%title%" />
		' : '
			<tr valign="middle">
				<td width="%width1%" height="20" nowrap>%_title%:</td>
				<td width="%width2%" ><input type="text" size="420" maxlength="250" name="title" value="%title%" /></td>
			</tr>
		';

		$tpl .= '
			<tr>
				<td></td>
				<td>%textarea%</td>
			</td>
			<tr valign="middle">
				<td>%_tags%:</td>
				<td><input type="text" size="420" maxlength="250" name="tags" value="%tags%"></td>
			</tr>
			<tr valign="middle">
				<td height="20"></td>
				<td><font color="#888888">%_tag-info%</font></td>
			</tr>
			<tr valign="middle">
				<td width="%width1%" height="20"></td>
				<td bgcolor="#3f3f3f">%tags-list%</td>
			</tr>
			<tr>
				<td height="10"></td>
				<td></td>
			</tr>
			<tr valign="middle">
				<td align="right">%cb-shared%&nbsp;</td>
				<td>%_shared%</td>
			</tr>
			<tr>
				<td height="4px"></td><td></td>
			</tr>
			<tr style="background-color: #181F2C" >
				<td colspan="2" valign="middle" align="center" height="40px">
					<input type="submit" value="%_btn-save%" />
				</td>
			</tr>
		</table>
		</form>
		';

		if ($note['id'] == -1) {
			$formDelete = '';
		} else {
			// Edit mode
			$tplDeleteForm = '
			<form action="%uri%" method="post">
				<input style="background-color: #450000" type="submit" value="%_btn-delete%">

				<input type="hidden" name="cid" value="%cid%" />
				<input type="hidden" name="id" value="%id%" />
				<input type="hidden" name="redirect" value="%redirect%" />
			</form>
			';

			$formDelete = strtr($tplDeleteForm, array(
				'%uri%' => SERVER.'?mode=askdel',
				'%_btn-delete%' => _h(_t('btn_delete')),
				'%id%' => $note['id'],
				'%cid%' => $note['cid'],
				'%redirect%' => _h($this->redirect),
			));
		}

		return strtr($tpl, array(
			'%str-header%' => str_replace('%form-delete%', $formDelete, $strHeader),

			'%uri%' => SERVER,
			'%_title%' => _h(_t('note_title')),
			'%_tags%' => _h(_t('tags')),
			'%_tag-info%' => _h(_t('multiple_tag_info')),
			'%_shared%' => _t('shared_for_all_characters_on_account'),
			'%_btn-save%' => _t('btn_save'),



			'%id%' => $note['id'],
			'%cid%' => $this->Notes->cid,
			'%title%' => _h($note['title']),
			'%tags%' => _h(implode(',', array_keys($noteTags))),
			'%tags-list%' => $tags_list,

			'%width1%' => $width1,
			'%width2%' => $width2,

			'%textarea%' => $this->htmlTextarea('contents', $note['contents']),
			'%cb-shared%' => $this->htmlCheckbox('shared', $note['shared'] == 1),

			'%redirect%' => _h($this->redirect),
		));
	}

	/**
	 * @param array $note
	 *
	 * @return string
	 */
	function renderDeleteNote($note) {
		$backUri = $this->redirect !== '' ? $this->redirect : SERVER.'?cid='.$this->Notes->cid;
		$btnBack = $this->renderBackLink($backUri);

		$tpl = '
		%btn-back%
		<form method="post" action="%uri%">
			<table width="100%">
			<tr>
				<td height="30px" align="center"><h2>%_confirm%</h2></td>
			</tr>
			<tr>
				<td align="center"><input type="submit" value=" %_btn_yes% " /></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td>%textarea%</td>
			</tr>
			</table>

			<p>%_title%: %title%</p>

			<input type="hidden" name="mode" value="del" />
			<input type="hidden" name="cid" value="%cid%" />
			<input type="hidden" name="id" value="%id%" />
			<input type="hidden" name="redirect" value="%redirect%" />
		</form>
		';
		return strtr($tpl, array(
			'%btn-back%' => $btnBack,

			'%uri%' => SERVER,
			'%_confirm%' => _h(_t('confirm_deletion')),
			'%_btn_yes%' => _h(_t('btn_yes')),

			'%_title%' => _h(_t('title')),
			'%title%' => _h($note['title']),
			'%textarea%' => $this->htmlTextarea('contents', $note['contents']),

			'%id%' => $note['id'],
			'%cid%' => $this->Notes->cid,
			'%redirect%' => _h($this->redirect),
		));
	}

	/**
	 * @param string $uri
	 *
	 * @return string
	 */
	protected function renderBackLink($uri) {
		$tpl = '
		<table width="100%">
		<tr valign="middle" bgcolor="#000000">
			<td height="25">&nbsp;<a href="%uri%">%_back%</a></td>
		</tr>
		</table>
		';
		return strtr($tpl, array(
			'%uri%' => $uri,
			'%_back%' => _h(_t('back')),
		));
	}

	/**
	 * @param string $name
	 * @param bool $checked
	 *
	 * @return string
	 */
	protected function htmlCheckbox($name, $checked) {
		return sprintf('<input type="checkbox" name="%s"%s>', $name, $checked ? ' checked="checked"' : '');
	}

	/**
	 * @param string $name
	 * @param string $contents
	 *
	 * @return string
	 */
	protected function htmlTextarea($name, $contents) {
		// Pad the ig textarea if the note has less than 5 lines
		$lineCount = substr_count($contents, "\n");
		$padding = ($this->ig && $lineCount < 5)
			? str_repeat("\n", max(0, 5 - $lineCount))
			: '';

		return sprintf('<textarea name="%s" rows="%d" cols="%d"%s>%s%s</textarea>',
			$name,
			ON_IPHONE ? 10 : 15,
			$this->ig ? 35 : 60,
			ON_IPHONE ? ' onfocus="sizeTextarea(this)" onkeyup="sizeTextarea(this)"' : '',
			_h($contents),
			$padding
		);
	}

}

